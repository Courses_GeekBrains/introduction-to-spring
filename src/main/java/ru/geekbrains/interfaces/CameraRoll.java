package ru.geekbrains.interfaces;

/**
 * Интерфейс Фотопленка
 */
public interface CameraRoll {
    public void processing();
}
