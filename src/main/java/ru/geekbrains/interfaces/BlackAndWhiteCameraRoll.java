package ru.geekbrains.interfaces;

/**
 * Черно-белая фотопленка
 */
public class BlackAndWhiteCameraRoll implements CameraRoll {
    public void processing() {
        System.out.println("-1 черно-белый кадр");
    }
}
