package ru.geekbrains;

import ru.geekbrains.interfaces.CameraRoll;

/**
 * Цветная фотопленка
 */
public class ColorCameraRoll implements CameraRoll {
    @Override
    public void processing() {
        System.out.println("-1 цветной кадр");
    }
}
