package ru.geekbrains;

import ru.geekbrains.interfaces.CameraRoll;

/**
 * Камера
 */
public class Camera {
    private CameraRoll cameraRoll;

    public CameraRoll getCameraRoll() {
        return cameraRoll;
    }

    public void setCameraRoll(CameraRoll cameraRoll) {
        this.cameraRoll = cameraRoll;
    }

    public void doPhotograph(){
        System.out.println("Щелк!");
        cameraRoll.processing();
    }

}
